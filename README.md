# English On The Go

This is a consturctor for new english lessons for students. New lesson should be created with .md extension and put into `lesson` folder. 

## Update from Gitlab

1. Open [Web IDE](https://gitlab.com/-/ide/project/peramor/english-on-the-go/edit/master/-/lessons/)
2. Create new file in the `lessons` folder from sidebar

![sidebar](./docs/new_file.png)

3. Name it with shorthand lesson command and .md extension, then click `Create file`.

![filename](./docs/file_name.png)

4. Add lesson's content, then click `Commit`

![file content](./docs/file_example.png)

5. Write a comment, choose `Commit to master branch`, then click `Commit`

![save](./docs/save.png)

6. Navigate to [Pipelines](https://gitlab.com/peramor/english-on-the-go/-/pipelines) to see the progress of updating on the server. It may take some time.

![progress](./docs/progress.png)

7. Check the bot when see the heavy checkmark
