const delimeter = '--------------------------------------------#####';
const contentTypes = ['audio', 'video', 'image'];
const actionTypes = ['recording'];

const btnRegexp = /(\[.+\])/gmi;
const contentRegexp = new RegExp(`\\((${contentTypes.join('|')})\\:.+\\)`, 'gmi');
const actionRegexp = new RegExp(`\\(${actionTypes.join('|')}\\)`, 'gmi');

const markdownToJson = (text) => {
  const [head, ...entries] = text.split(delimeter);

  const descriptors = head.trim().split('\n').reduce((acc, cur) => {
    const [key, value] = cur.split(':');
    acc[key.toLowerCase().trim().replace(/\s/g, '_')] = value.trim();
    return acc;
  }, {});

  const replicas = entries.map(entry => {
    let text = entry;

    const [content] = contentRegexp.exec(text) || [];
    text = entry.replace(contentRegexp, '');

    const [buttons] = btnRegexp.exec(text) || [];
    text = text.replace(btnRegexp, '');

    const [action] = actionRegexp.exec(text) || [];
    text = text.replace(actionRegexp, '');

    const [contentType, contentPath] = content?.slice(1,-1).trim().split(': ') || [];

    return {
      text: text.trim().replace(/\n{3,}/gm, '\n\n'), 
      contentType,
      contentPath, 
      buttons: buttons?.slice(1,-1).split(','), 
      action: action?.slice(1,-1).trim(),
    }
  });

  return {
    ...descriptors,
    replicas,
  }
}

module.exports = {
  markdownToJson,
}