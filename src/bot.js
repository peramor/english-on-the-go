const { Telegraf } = require('telegraf');
const Stage = require('telegraf/stage');
const Scene = require('telegraf/scenes/base');
const session = require('telegraf/session');
const glob = require('glob');
const { markdownToJson } = require('./md-to-json');
const fs = require('fs');
const WizzardScene = require('telegraf/scenes/wizard');
const { compose } = require('./composer');
const Koa = require('koa');
const koaBody = require('koa-body');

// Send the /id command to the bot to know your ID, and change it:
const coachId = 123;
const token = process.env.TOKEN;

async function bootstrap() {
  const bot = new Telegraf(token);
  bot.use(session());
  bot.on('audio', ctx => ctx.reply('Audio File ID: ' + ctx.message.audio.file_id));
  bot.on('video', ctx => ctx.reply('Video File ID: ' + ctx.message.video.file_id));
  bot.on('photo', ctx => ctx.reply('Image File ID: ' + ctx.message.photo[0].file_id));
  bot.command('id', ctx => ctx.reply('ID: ' + ctx.from.id));

  const mainScene = new Scene('main');

  mainScene.enter(ctx => {
    // TODO: the start phrase may be changed
    ctx.reply("Hello, human. I will assist your teacher with lessons. He should send me a coomand to start a lesson.");
    ctx.scene.reset();
  });

  const filepathes = await new Promise((res, rej) =>
    glob('./lessons/*.md', (err, path) => err ? rej(err) : res(path))
  );

  console.log('lessons loaded: ', filepathes);

  const lessons = filepathes.map(filepath =>
    markdownToJson(fs.readFileSync(filepath, 'utf-8'))
  );

  const scenes = lessons.map(lesson => {
    return new WizzardScene(
      lesson.command,
      ...compose(lesson),
    );
  });

  const stage = new Stage([mainScene, ...scenes]);

  stage.start(ctx => ctx.scene.enter('main'));

  stage.command('finish', ctx => {
    ctx.reply("OK. Let's finish the lesson. Send new one when you are ready.", {
      reply_markup: {
        remove_keyboard: true,
      }
    });
    ctx.scene.enter('main', null, true);
  });

  lessons.forEach(lesson => {
    stage.command(lesson.command, ctx => ctx.scene.enter(lesson.command));
  });

  bot.use(stage.middleware());

  bot.catch((err) => console.log(err));

  if (process.env.LOCAL)
    bot.startPolling();

  // just for heroku 

  const app = new Koa();
  app.use(koaBody());
  app.use(async (ctx, next) => {
    if (ctx.method !== 'POST' || ctx.url !== '/secret-path') {
      return next()
    }
    await bot.handleUpdate(ctx.request.body, ctx.response)
    ctx.status = 200
  })

  const port = process.env.PORT || 3000;
  app.listen(port, () => {
    console.log(`app is listening on ${port} port`);
    console.log('bot started');
  });
}

bootstrap();

