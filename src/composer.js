const nextButton = (caption) => ({
  reply_markup: {
    keyboard: [[{ text: caption }]],
    resize_keyboard: true,
    one_time_keyboard: true,
  }
});

const compose = (json) => {
  const lastIndx = json.replicas.length - 1;

  return json.replicas.map((replica, i) => {
    const {text, contentType, contentPath, buttons, action} = replica;
    return async (ctx) => {
      if (ctx.wizard.state.activeAction === 'button') {
        if (ctx.message?.text !== ctx.wizard.state.waitForText) return;
      } else if (ctx.wizard.state.activeAction === 'recording') {
        if (!ctx.message?.voice) return;
      }

      if (contentType) {
        contentType === 'audio' && await ctx.replyWithAudio(contentPath);
        contentType === 'video' && await ctx.replyWithVideo(contentPath);
        contentType === 'image' && await ctx.replyWithPhoto(contentPath);
      }

      await ctx.replyWithMarkdown(text, buttons && nextButton(buttons[0]));

      ctx.wizard.state.activeAction = buttons ? 'button' : action;
      ctx.wizard.state.waitForText = buttons ? buttons[0] : null;

      if (i === lastIndx) {
        ctx.reply('Great. The lesson is finished.', {
          reply_markup: {
            remove_keyboard: true,
          }
        });
        return ctx.scene.enter('main', null, true);
      }
      else 
        return ctx.wizard.next();
    }
  });
}

module.exports = {
  compose,
}