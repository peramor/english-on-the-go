Lesson name: I'am
Author: William
Command: iam

--------------------------------------------#####

*O uso de* _I’m (I am)_ *no Inglês*  
Nesta lição aprenderemos o uso de *I’m (I am)* no Inglês  
—  
Escreva ou clique em *OK* para continuar:
[OK]

--------------------------------------------#####

*I’m* é uma abreviação de *I am*, que em português significa: *Eu sou/Eu estou*.  
Assim, usamos essa construção para indicar alguns fatos sobre nós, tais como _onde estamos_, _o que estamos fazendo_, ou _qual é nosso estado naquele momento_. 
—
Escreva ou clique em *OK* para continuar:
[OK]

--------------------------------------------#####

Vamos ver alguns exemplos e exercitar. Siga os seguintes passos:
_Ouça o áudio_
_Grave uma mensagem repetindo o que você ouviu_
_Verifique o que foi falado_
—
Vamos lá? Escreva ou clique em *OK* para continuar:
[OK]

--------------------------------------------#####

*Ouça*
(audio: 1)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m so tired* - _estou tão cansado_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 2)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m confused* - _estou confuso_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 3)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m happy* - _estou feliz_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 4)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m twenty three years old* - _sou vinte e três anos velho_

*Nota*: No Inglês você se fala *eu tenho* tantos anos, mas sim *eu sou* tantos anos.
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 5)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m hungry* - _estou com fome_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 6)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m nervous* - _estou nervoso_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 7)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m excited* - _estou empolgado_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 8)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m leaving work* - _estou saindo do trabalho_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 9)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m thirsty* - _estou com sede_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 10)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m from Seattle* - _sou de seattle_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 11)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m extremely tired* - _estou extremamente cansado_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 12)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m very happy* - _estou muito feliz_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 13)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m terribly hungry* - _estou terrivelmente faminto_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 14)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m super excited* - _estou super empolgado_
—
Escreva ou clique em *PRÓXIMO* para continuar:
[PRÓXIMO]

--------------------------------------------#####

*Ouça*
(audio: 15)

*Grave sua repetição!*
(recording)

--------------------------------------------#####

*I’m very nervous* - _estou muito nervoso_
