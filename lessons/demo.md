Lesson name: Demo
Author: Roman Maltsev
Command: demo

--------------------------------------------#####

This lesson is created to show how the bot works.
The main feature of the bot to convert Markdown files to actual lessons.
You just have to follow specific format of Markdown file, like this one.
—
To add a button to your text, just put the button name to square breackets:
[This is a button, push me]

--------------------------------------------#####

Each message is separated by the delimeter --##
You have to copy paste the delimeter between messages, because it must contain the same amount of "-" and "#", otherwise it won't work.
[Continue]

--------------------------------------------#####

For now, you may only use a single button, and it will always move you to a next message.
[No problem]

--------------------------------------------#####

Instead of a button, you may request your students to record a voice message for moving to a next exercise or message. Just put a specific work in round brackets, but don't combain "recording" command with buttons (behavior is unpredictable in such cases, so the programmer is not responsible for this). 
(recording)

To continue, your student have to record a voice message

--------------------------------------------#####

To add some fancy, you may use files in your messages like images, videos or audios.
[Tell me more]

--------------------------------------------#####

(image: AgACAgIAAxkBAAIDD2CJjnTq_Q047DC8xxFCMnok7rKAAAJIsTEblnhRSFhwDmtymwTQHbWZoS4AAwEAAwIAA20AA6OWAQABHwQ)

This is example of an image. The syntax must be as follow:
(filetype: file id)

You may use the following values as filetype:
> `image`
> `video`
> `audio`
[Next]

--------------------------------------------#####

To get a file ID, simply send a file to the bot, he will response with file ID which you can put as a value in round brackets.
Try to send any image from your device (withour comprasion, please), then push the button to continue:
[Continue]

--------------------------------------------#####

_This is an example of a sample task for a student_: 

Please, record a voice message, try to predict what city on the image, and why do you think so?
(image: AgACAgIAAxkBAAIDD2CJjnTq_Q047DC8xxFCMnok7rKAAAJIsTEblnhRSFhwDmtymwTQHbWZoS4AAwEAAwIAA20AA6OWAQABHwQ)
(recording)

--------------------------------------------#####

This is the end of tutorial.
It is not required to put any actions in the last message, the bot will say that the lesson is finished after the message.